import { Observable, ReplaySubject } from 'rxjs';

/**
 * Fonction permettant d'encapsuler le résultat d'un Observable dans un ReplaySubject
 * de sorte à retourner la dernière valeur stockée dans cet objet.
 * @param observable  Observable à capturer
 */
export function cacheable<T>(observable: Observable<T>): Observable<T> {
  // Buffer de taille 1 pour toujorus retourner la dernière valeur émise par l'Observable
  const subject = new ReplaySubject<T>(1);
  observable.subscribe(
    x => subject.next(x),
    x => subject.error(x),
    () => subject.complete()
  );
  return subject.asObservable();
}
