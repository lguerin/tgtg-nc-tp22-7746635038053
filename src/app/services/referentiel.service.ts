import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Cacheable } from '../decorators/cacheable.decorator';

@Injectable({
  providedIn: 'root'
})
export class ReferentielService {

  constructor(private http: HttpClient) { }

  /**
   * Récupérer les catégories des commerces
   */
  @Cacheable('categories')
  getStoreCategories(): Observable<Array<string>> {
    return this.http.get<Array<string>>(`${environment.baseURL}/api/referentiels/categories`);
  }

  /**
   * Récupérer les labels existants sur les commerces
   */
  @Cacheable('labels')
  getStoreLabels(): Observable<Array<string>> {
    return this.http.get<Array<string>>(`${environment.baseURL}/api/referentiels/labels`);
  }
}
