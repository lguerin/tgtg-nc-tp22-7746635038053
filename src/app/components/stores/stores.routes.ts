import { Routes } from '@angular/router';
import { StoresComponent } from './stores.component';
import { StoreComponent } from './store/store.component';
import { StoreResolver } from '../../resolvers/store.resolver';
import { StoreFormComponent } from './store/store-form/store-form.component';

export const STORES_ROUTES: Routes = [
  { path: '', component: StoresComponent },
  { path: 'create', pathMatch: 'full', component: StoreFormComponent },
  { path: ':id', component: StoreComponent, resolve: { store: StoreResolver } },
  { path: ':id/edit', component: StoreFormComponent, resolve: { store: StoreResolver } }
];
