import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'nc-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  email: FormControl;
  password: FormControl;
  hasError = false;

  constructor(private fb: FormBuilder,
              private authenticationService: AuthenticationService,
              private router: Router,
              private translateService: TranslateService) { }

  ngOnInit(): void {
    this.initLoginForm();
  }

  getErrorMessage(control: FormControl): string {
    let error = '';
    if (control.hasError('required')) {
      error = this.translateService.instant('validation.error.required');
    }
    if (control.hasError('email')) {
      error = this.translateService.instant('validation.error.email');
    }
    return error;
  }

  /**
   * Initialise le formulaire de login
   */
  initLoginForm(): void {
    // Init des champs
    this.email = this.fb.control('', [Validators.required, Validators.email]);
    this.password = this.fb.control('', [Validators.required]);

    // Build du formulaire
    this.loginForm = this.fb.group({
      email: this.email,
      password: this.password
    });
  }

  /**
   * Authentifier un utilisateur
   */
  authenticate(): void {
    this.hasError = false;
    const form = this.loginForm.value;
    this.authenticationService.authenticate(form.email, form.password).subscribe(
      () => this.router.navigate(['/']),
      () => this.hasError = true
    );
  }
}
