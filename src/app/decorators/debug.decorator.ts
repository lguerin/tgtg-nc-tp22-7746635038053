/* tslint:disable:typedef only-arrow-functions */

export function Debug() {
  return function(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>): void {
    const originalMethod = descriptor.value;
    descriptor.value = function(...args: any[]) {
      let msg = `[DEBUG] Appel de la méthode '${propertyKey}' avec les params: `;
      args.forEach(arg => {
        if (arg) {
          msg += JSON.stringify(arg) + ' ';
        }
      });
      console.log(msg);
      return originalMethod.apply(this, args);
    };
  };
}
