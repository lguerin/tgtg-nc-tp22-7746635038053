/* tslint:disable:typedef only-arrow-functions */

import { cacheable } from '../functions/cacheable.function';
import { Observable } from 'rxjs';

export function Cacheable(name: string) {
  return function(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>): void {
    const originalMethod = descriptor.value;
    const key = '__cacheable';
    target[key] = new Map<string, Observable<any>>();

    descriptor.value = function(...args: any[]) {
      const map = target[key] as Map<string, Observable<any>>;
      if (!map.has(name)) {
        console.log(`[cacheable] Put collection of ${name} into the cache`);
        map.set(name, cacheable(originalMethod.apply(this, args)));
      } else {
        console.log(`[cacheable] Get collection of ${name} from the cache`);
      }
      return map.get(name);
    };
  };
}
